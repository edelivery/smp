package pages.administration.editResourcesPage.editResourceDocumentPage;

import ddsl.commonPages.commonDocumentPage.CommonSelectReferenceDialog;
import org.openqa.selenium.WebDriver;

public class SelectResourceDocumentDialog extends CommonSelectReferenceDialog {
    public SelectResourceDocumentDialog(WebDriver driver) {
        super(driver);
    }
}
